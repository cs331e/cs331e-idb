import os
import urllib.request
import urllib.parse
import json
import io
import unittest
from test import Driver_TestCases, Track_TestCases, Organization_TestCases, Competed_TestCases, Driverart_TestCases, Trackart_TestCases, Orgart_TestCases
from flask import Flask, render_template, request, send_from_directory, jsonify
from create_db import app, db, Organization, create_Organization, Orgart, create_Orgart, Driver, create_Drivers, Driverart, Track, create_Tracks, Trackart, create_Trackart, Competed, Issue, Commit, Unit
# app = Flask(__name__, static_folder="./frontend/build/static", template_folder="./frontend/build")
# CORS(app)


# Using the defualt path to update the information dictionary with pull requests
@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def index(path):
    # players = create_players_list()
    # teams = create_teams_list()
    # file = open("info2.txt", "w")
    # file.write(str(players)+'\n'*2+str(teams))
    # file.close()
    return render_template('index.html')


@app.route('/api/drivers')
def drivers():
    result = [{'driver': item[0].serialize, 'team': item[1].serialize}
              for item in db.session.query(Driver, Organization).join(Organization).all()]
    return jsonify(result)


@app.route('/api/orgs')
def orgs():
    result = [item.serialize for item in db.session.query(Organization).all()]
    return jsonify(result)


@app.route('/api/tracks')
def tracks():
    result = [item.serialize for item in db.session.query(Track).all()]
    return jsonify(result)


@app.route('/api/driver/<int:id>')
def driver(id):
    temp = db.session.query(Driver, Organization).filter_by(
        idDriver=id).join(Organization).first()
    result = {'driver': '',
              'team': ''}
    result['driver'] = temp[0].serialize
    result['team'] = temp[1].serialize
    return result


@app.route('/api/org/<int:id>')
def org(id):
    result = Organization.query.filter_by(idTeam=id).first().serialize
    return result


@app.route('/api/track/<int:id>')
def track(id):
    result = Track.query.filter_by(idTrack=id).first().serialize
    return result


@app.route("/api/test")
def test():
    output = io.StringIO()
    test_list = [Driver_TestCases, Organization_TestCases, Track_TestCases,
                 Competed_TestCases, Driverart_TestCases, Orgart_TestCases, Trackart_TestCases]
    loader = unittest.TestLoader()
    suites_list = []
    for test_class in test_list:
        suite = loader.loadTestsFromTestCase(testCaseClass=test_class)
        suites_list.append(suite)
    big_suite = unittest.TestSuite(suites_list)
    unittest.TextTestRunner(stream=output).run(big_suite)
    result = output.getvalue()
    output.close()
    return result


@app.route("/api/competeds")
def competeds():
    result = [item.serialize for item in db.session.query(Competed).all()]
    return jsonify(result)


@app.route("/api/gitlab")
def gitlab():
    Ted = {'issues': 0, 'commits': 0, 'units': 0}
    Eddie = {'issues': 0, 'commits': 0, 'units': 0}
    Christina = {'issues': 0, 'commits': 0, 'units': 0}
    Jungwoo = {'issues': 0, 'commits': 0, 'units': 0}
    Kevin = {'issues': 0, 'commits': 0, 'units': 0}
    issues = [item.serialize for item in db.session.query(Issue).all()]
    issueLen = len(issues)
    for issue in issues:
        if issue['user'] == 'coolical':
            Kevin['issues'] += 1
            pass
        if issue['user'] == 'eddiecasti280':
            Eddie['issues'] += 1
            pass
        if issue['user'] == 'chrpeebles':
            Christina['issues'] += 1
            pass
        if issue['user'] == 'Jungwoo-Joo-99':
            Jungwoo['issues'] += 1
            pass
        if issue['user'] == 'Theopy':
            Ted['issues'] += 1
            pass
    commits = [item.serialize for item in db.session.query(Commit).all()]
    commitsLen = len(commits)
    for commit in commits:
        if commit['user'] == 'coolical' or commit['user'] == 'Kevin Woo':
            Kevin['commits'] += 1
        if commit['user'] == 'eddiecasti280' or commit['user'] == 'Eddie Castillo':
            Eddie['commits'] += 1
        if commit['user'] == 'chrpeebles' or commit['user'] == 'Christina Peebles':
            Christina['commits'] += 1
        if commit['user'] == 'Jungwoo-Joo-99' or commit['user'] == "Jungwoo Joo":
            Jungwoo['commits'] += 1
        if commit['user'] == 'Theopy' or commit['user'] == "Theodore Johnson":
            Ted['commits'] += 1
    units = [item.serialize for item in db.session.query(Unit).all()]
    unitCount = 0
    for unit in units:
        unitCount += unit['count']
    Jungwoo['units'] = units[0]['count']
    Kevin['units'] = units[1]['count']
    Christina['units'] = units[2]['count']
    Eddie['units'] = units[3]['count']
    Ted['units'] = units[4]['count']
    return {'jungwoo': Jungwoo, 'kevin': Kevin, 'christina': Christina, 'eddie': Eddie, 'ted': Ted, 'issues': issueLen, 'commits': commitsLen, 'units': unitCount}


@app.route("/api/search/<string:name>")
def search(name):
    name = "%{}%".format(name)
    result = []
    temp = Driver.query.filter(Driver.name.ilike(name)).all()
    result.extend([item.serialize for item in temp])
    temp = Organization.query.filter(Organization.name.ilike(name)).all()
    result.extend([item.serialize for item in temp])
    temp = Track.query.filter(Track.name.ilike(name)).all()
    result.extend([item.serialize for item in temp])
    return jsonify(result)


def create_teams_list():
    # following same process as player list
    url1 = 'https://www.thesportsdb.com/api/v1/json/1/searchteams.php?t=Red_Bull'
    url2 = 'https://www.thesportsdb.com/api/v1/json/1/searchteams.php?t=Mercedes'
    url3 = 'https://www.thesportsdb.com/api/v1/json/1/searchteams.php?t=McLaren'
    drivers = []
    links = [url1, url2, url3]
    teams = []
    for url in links:
        f = urllib.request.urlopen(url)
        unclean = f.read().decode('utf-8')
        clean = (json.loads(unclean))
        clean = dict(eval(str(clean['teams']))[0])

        bad_keys = []
        for keys in clean:
            if clean[keys] is None or clean[keys] == '':
                bad_keys.append(keys)
            elif keys[-2:] in ['FR']:
                bad_keys.append(keys)
        for keys in bad_keys:
            del clean[keys]
        clean['strDescriptionEN'] = clean['strDescriptionEN'][: clean['strDescriptionEN'].find(
            '.', clean['strDescriptionEN'].find('.') + 1)+1]
        teams.append(clean)
    return teams


def create_players_list():
    # Examples of requests from sports DB with dev id, we can get more info and better requests for $5 if we want
    # For phase 2, once we get more data we can pull players of list from ever team, then loop through those and
    # collect entire pages for ever player from thesportsdb
    url1 = 'https://www.thesportsdb.com/api/v1/json/1/searchplayers.php?p=Max_Verstappen'
    url2 = 'https://www.thesportsdb.com/api/v1/json/1/searchplayers.php?p=Valtteri_Bottas'
    url3 = 'https://www.thesportsdb.com/api/v1/json/1/searchplayers.php?p=Lando_Norris'
    drivers = []
    links = [url1, url2, url3]
    for url in links:
        f = urllib.request.urlopen(url)
        unclean = f.read().decode('utf-8')
    # Currently the input in formatted in this a string {"player":[{'idPlayer':######',.....,'strLocked':'****'}]}
    # basically we are only insterest in that inner dictionary, so we can chop off the out pieces, cast to dict,
    # then cull through the dictionary, leaving usable information only
        clean = eval(unclean[11:-2].replace('null', "'null'"))
        bad_keys = []
        for keys in clean:
            # here we go through each key, removing elements of the dictionary that are null, in other langues, or
            # extraneous. We collect the dirty keys and then remove them to avoid the dictionary changing size during
            # iteration
            if clean[keys] in ['null', '']:
                bad_keys.append(keys)
            elif keys[-2:] in ['FR']:
                bad_keys.append(keys)
        for keys in bad_keys:
            del clean[keys]
    # this is just cleaning the description from the api to only be two sentences
        clean['strDescriptionEN'] = clean['strDescriptionEN'][: clean['strDescriptionEN'].find(
            '.', clean['strDescriptionEN'].find('.') + 1)+1]
        drivers.append(clean)
    return drivers

# The project instructions mention that there has to be some order to items


def organize_items(item_arr, key, ascending):
    if ascending:
        return sorted(item_arr, key=lambda x: (x[key]))
    else:
        return sorted(item_arr, key=lambda x: (x[key]), reverse=True)


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80, threaded=True, debug=True)
