import React, { useState } from 'react';
import { Navbar, Nav, Form, FormControl, Button } from 'react-bootstrap';
import { BrowserRouter as Router, Route, useParams, Link, Switch, Redirect } from "react-router-dom";
import useAxios from 'axios-hooks'
import '../style/style.css';
import logo from '../logo.png';
import Driver from './driver';
import Drivers from './drivers';
import Tracks from './tracks';
import Home from './home';
import Track from './track';
import Orgs from './orgs';
import Org from './org';
import About from './about';
import Search from './search';

export default function App() {
  const [search, setSearch] = useState("");

  return (
    <div id="home_page">
      <Router>
        <Navbar sticky="top" bg="light" variant="light">
          <Navbar.Brand href="/">
            <img
              src={logo}
              width="30"
              height="30"
              className="d-inline-block align-top"
              alt="Donut with Checkered glaze"
            />
          </Navbar.Brand>
          <Nav className="mr-auto">
            <Nav.Link as={Link} to="/">Home</Nav.Link>
            <Nav.Link as={Link} to="/about">About</Nav.Link>
            <Nav.Link as={Link} to="/drivers">Drivers</Nav.Link>
            <Nav.Link as={Link} to="/tracks">Tracks</Nav.Link>
            <Nav.Link as={Link} to="/teams">Teams</Nav.Link>
            {/* <Nav.Link disabled as={Link} to="/leagues">Leagues</Nav.Link> */}
          </Nav>
          <Form inline onSubmit={(e) => { e.preventDefault(); }}>
            <FormControl type="text" placeholder="Search" className="mr-sm-2" onChange={e => setSearch(e.target.value)}></FormControl>
            <Link to={"/search/" + search}>
              <Button variant="outline-primary" disabled={!search}>Search</Button>
            </Link>
          </Form>
        </Navbar>
        <Switch>
          <Route exact path="/driver/:id">
            <Driver />
          </Route>
          <Route exact path="/track/:id">
            <Track />
          </Route>
          <Route exact path="/team/:id">
            <Org />
          </Route>
          <Route exact path="/">
            <Home />
          </Route>
          <Route exact path="/about">
            <About />
          </Route>
          <Route exact path="/drivers">
            <Drivers />
          </Route>
          <Route exact path="/tracks">
            <Tracks />
          </Route>
          <Route exact path="/teams">
            <Orgs />
          </Route>
          {/* <Route exact path="/leagues">
            <Home />
          </Route> */}
          <Route exact path="/search">
            <Redirect to="/"></Redirect>
          </Route>
          <Route path="/search/:search">
            <Search></Search>
          </Route>
          <Redirect to="/"></Redirect>
        </Switch>
      </Router>
    </div>
  )
}

