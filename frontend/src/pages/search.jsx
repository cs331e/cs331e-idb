import useAxios from "axios-hooks";
import React, { useState, useEffect } from "react";
import { CardDeck, Container, ListGroup, ListGroupItem, Nav, Button, ToggleButtonGroup, Row, Navbar, ButtonGroup, Card, Pagination } from 'react-bootstrap';
import { BrowserRouter as Router, Route, useParams, Link } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import '../style/custom.css';
import { useBootstrapPrefix } from "react-bootstrap/esm/ThemeProvider";
import logo from '../logo.png';
import bitten from '../images/bitten.PNG';
// const drivers = [{ 'id': 0, 'name': 'Max Verstappen', 'birth_loc': 'Hasselt, France', 'birth_yr': '1997', 'team': { 'name': 'Red Bull', 'id': 2 }, 'latest results': [{ 'id': 0, 'str': 'First Place, Abu Dhabi Grand Prix' }, { 'id': 1, 'str': 'Second Place, Bahrain Grand Prix' }, { 'id': 2, 'str': 'Sixth Place, Turkish Grand Prix' }], 'image': 'https://www.thesportsdb.com/images/media/player/cutout/ad31ne1587471775.png' },
// { 'id': 1, 'name': 'Valtteri Bottas', 'birth_loc': 'Nastola, Finland', 'birth_yr': '1989', 'team': { 'name': 'Mercedes', 'id': 0 }, 'latest results': [{ 'id': 0, 'str': 'Second Place, Abu Dhabi Grand Prix' }, { 'id': 1, 'str': 'Eighth Place, Bahrain Grand Prix' }, { 'id': 2, 'str': 'Fourteenth Place, Turkish Grand Prix' }], 'image': 'https://www.thesportsdb.com/images/media/player/cutout/slwoj91587472585.png' },
// { 'id': 2, 'name': 'Lando Norris', 'birth_loc': 'Bristol, England', 'birth_yr': '1999', 'team': { 'name': 'McLaren', 'id': 1 }, 'latest results': [{ 'id': 0, 'str': 'Fifth Place, Abu Dhabi Grand Prix' }, { 'id': 1, 'str': 'Fourth Place, Bahrain Grand Prix' }, { 'id': 2, 'str': 'Eighth Place, Turkish Grand Prix' }], 'image': 'https://www.thesportsdb.com/images/media/player/cutout/0t83vs1587468873.png' }];

function Search() {
    const {search} = useParams();
    const [asc, setOrder] = useState(true);
    const [page, setPage] = useState(1);
    const [sortType, setSortType] = useState('name');
    const [{ data: data, loading: loading, error: error }] = useAxios("/api/search/"+search);
    const [list, setList] = useState([]);
    let pages = [];
    if (data != null) {
        var len = data.length / 6;
        if (data.length % 6 != 0) {
            len += 1;
        }
        for (let num = 1; num <= len; num++) {
            pages.push(
                <Pagination.Item key={num} active={num === page} onClick={() => setPage(num)}>
                    {num}
                </Pagination.Item>
            )
        }
    }
    useEffect(() => {
        async function sortArray(type) {
            await setList(data.slice((page - 1) * 6, page * 6));
        };
        if (data != null) {
            sortArray(sortType);
        }
    }, [sortType, data, page, asc]);
    if (loading) {
        return (<Container className="home_content">
            <img src={logo} alt="Donut logo with racetrack icing" className="App-logo" width="75%" />
            <h1>Loading...</h1>
        </Container>);
    }
    if (error) {
        return (<Container className="home_content">
            <img src={bitten} alt="Bitten donut with racetrack icing" className="App-logo" width="75%" />
            <h1>Error!</h1>
        </Container>);
    }
    if (data.length == 0){
        return (<Container className="home_content">
            <img src={bitten} alt="Bitten donut with racetrack icing" className="App-logo" width="75%" />
            <h1>No results found...</h1>
        </Container>);
    }
    return (
        <div>
            <Container>
                <CardDeck class="row col-xs justify-content-center" id="cards">
                    {list.map(item => (
                        <div class="col-sm-3 card gx-3 m-3" key={item.id} style={{ width: 18 + "rem" }}>
                            <img class="card-img-top img-thumbnail" src={('idDriver' in item ? item.driverart: ('idTeam' in item ? item.orgart: ('idTrack' in item ? item.trackart: '/')))} alt={('idDriver' in item ? item.driverart: ('idTeam' in item ? item.orgart: ('idTrack' in item ? item.trackart: 'Unknown')))} />
                            <div class="card-body">
                                <Link to={('idDriver' in item ? "/driver/"+item['idDriver']: ('idTeam' in item ? "/team/"+item['idTeam']: ('idTrack' in item ? "/track/"+item['idTrack']: '/')))}><h3 class="card-title">{item.name}</h3></Link>
                            </div>
                        </div>
                    )
                    )}
                </CardDeck>
            </Container>
            <Card class="col-md-12 text-center position-static" sticky="bottom" >
                <Pagination class="text-center">{pages}</Pagination>
            </Card>
        </div>
    );
}
export default Search;