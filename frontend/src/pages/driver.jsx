import useAxios from "axios-hooks";
import React from "react";
import { Card, ListGroup, ListGroupItem, Image, Container, Row, Col} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../style/custom.css';
import logo from '../logo.png';
import { useParams, Link } from "react-router-dom";
const drivers = [{ 'id': 0, 'name': 'Max Verstappen', 'birth_loc': 'Hasselt, France', 'birth_yr': '1997', 'team': { 'name': 'Red Bull', 'id': 2 }, 'latest results': [{ 'id': 0, 'str': 'First Place, Abu Dhabi Grand Prix' }, { 'id': 1, 'str': 'Second Place, Bahrain Grand Prix' }, { 'id': 2, 'str': 'Sixth Place, Turkish Grand Prix' }], 'image': 'https://www.thesportsdb.com/images/media/player/cutout/ad31ne1587471775.png' },
{ 'id': 1, 'name': 'Valtteri Bottas', 'birth_loc': 'Nastola, Finland', 'birth_yr': '1989', 'team': { 'name': 'Mercedes', 'id': 0 }, 'latest results': [{ 'id': 0, 'str': 'Second Place, Abu Dhabi Grand Prix' }, { 'id': 1, 'str': 'Eighth Place, Bahrain Grand Prix' }, { 'id': 2, 'str': 'Fourteenth Place, Turkish Grand Prix' }], 'image': 'https://www.thesportsdb.com/images/media/player/cutout/slwoj91587472585.png' },
{ 'id': 2, 'name': 'Lando Norris', 'birth_loc': 'Bristol, England', 'birth_yr': '1999', 'team': { 'name': 'McLaren', 'id': 1 }, 'latest results': [{ 'id': 0, 'str': 'Fifth Place, Abu Dhabi Grand Prix' }, { 'id': 1, 'str': 'Fourth Place, Bahrain Grand Prix' }, { 'id': 2, 'str': 'Eighth Place, Turkish Grand Prix' }], 'image': 'https://www.thesportsdb.com/images/media/player/cutout/0t83vs1587468873.png' }];
function Driver() {
    const { id } = useParams();
    const [{data, loading, error}] = useAxios("/api/driver/" + id);
    if (loading == true){
        return (<Container className="home_content">
            <img src={logo} alt="Donut logo with racetrack icing" className="App-logo" width="40%" />
            <h1>Loading...</h1>
        </Container>);
    }
    if (error == true){
        return <p>Error</p>
    }
    if (!loading && data != null){
        return (
            <Container className="about_proj home_content">
                <Row className="justify-content-center">
                    <Col md={6}>
                        <Image src={data.driver.driverart} alt={data.driver.driverart} roundedCircle fluid />
                    </Col>
                </Row>
                <h1>{data.driver.name}</h1>
                <Link class='team-link' to={'/team/' + data.driver.idTeam}>{data.team.name}</Link>

                <Card className="border-0">
                    <Card.Body>
                        {data.driver.description}
                    </Card.Body>
                </Card>

                <br></br>
    
                <Row>
                    <Col md={6}>
                        <Card>
                            <Card.Body>
                                <Card.Title class="card-title">Driver Data</Card.Title>
                            </Card.Body>
                            <ListGroup className="list-group-flush">
                                <ListGroupItem>
                                    <strong>Nationality:</strong> {data.driver.nationality}
                                </ListGroupItem>
                                <ListGroupItem>
                                    <strong>Gender:</strong> {data.driver.gender}
                                </ListGroupItem>
                                <ListGroupItem>
                                    <strong>Birthday:</strong> {data.driver.birthday}
                                </ListGroupItem>
                                <ListGroupItem>
                                    <strong>Birth Location:</strong> {data.driver.birth_loc}
                                </ListGroupItem>
                            </ListGroup>
                        </Card>
                    </Col>
                    <Col md={6}>
                        <Card>
                            <Card.Body>
                                <Card.Title class="card-title">Latest Results</Card.Title>
                            </Card.Body>
                            <ListGroup className="list-group-flush">
                                {data.driver.tracks.map(race => (
                                    <ListGroupItem>
                                        {race.event}: {race.result}
                                        <br></br>
                                        <Link class='d-link' to={'/track/' + race.idTrack}>{race.track}</Link>
                                    </ListGroupItem>
                                ))}
                            </ListGroup>
                        </Card>
                    </Col>
                </Row>
                <br></br>
            </Container>
        );
    }
}
export default Driver;