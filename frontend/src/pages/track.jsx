import useAxios from "axios-hooks";
import React from "react";
import { ListGroup, ListGroupItem, Card, Button, Row, Col, Container, Image } from 'react-bootstrap';
import ReactPlayer from "react-player";
import { useParams, Link } from "react-router-dom";
import logo from '../logo.png';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../style/custom.css';
const tracks = [{ 'id': 0, 'name': 'Abu Dhabi Grand Prix', 'loc': 'Abu Dhabi, UAE', 'date': '12/13/20', 'circuit': 'Yas Marina Circuit', 'competitors': [{ 'name': 'Max Verstappen', 'id': 0 }, { 'name': 'Valtteri Bottas', 'id': 1 }, { 'name': 'Lando Norris', 'id': 2 }], 'image': 'https://www.thesportsdb.com/images/media/event/thumb/s22twe1610814532.jpg' },
{ 'id': 1, 'name': 'Bahrain Grand Prix', 'loc': 'Sakhir, Bahrain', 'date': '11/29/20', 'circuit': 'Bahrain International Circuit', 'competitors': [{ 'name': 'Max Verstappen', 'id': 0 }, { 'name': 'Valtteri Bottas', 'id': 1 }, { 'name': 'Lando Norris', 'id': 2 }], 'image': 'https://www.thesportsdb.com/images/media/event/thumb/8l6v3s1610814042.jpg' },
{ 'id': 2, 'name': 'Turkish Grand Prix', 'loc': 'Istanbul, Turkey', 'date': '11/15/20', 'circuit': 'Intercity Istanbul Park', 'competitors': [{ 'name': 'Max Verstappen', 'id': 0 }, { 'name': 'Valtteri Bottas', 'id': 1 }, { 'name': 'Lando Norris', 'id': 2 }], 'image': 'https://www.thesportsdb.com/images/media/event/thumb/ty76f41605469172.jpg' }];
function Track() {
    const { id } = useParams();
    const [{ data, loading, error }] = useAxios("/api/track/" + id);

    if (loading) {
        return (<Container className="home_content">
            <img src={logo} alt="Donut logo with racetrack icing" className="App-logo" width="40%" />
            <h1>Loading...</h1>
        </Container>);
    }
    if (error) {
        return (<p>Error</p>)
    }
    if (!loading && data != null) {
        return (
            <Container className="about_proj home_content">
                <Row className="justify-content-center">
                    <Col md={6}>
                        <Image src={data.trackart} alt={data.trackart} roundedCircle fluid />
                    </Col>
                </Row>
                <h1>{data.name}</h1>
                <br></br>

                <Row>
                    <Col md={6}>
                        <Card>
                            <Card.Body>
                                <Card.Title class="card-title">Track Data</Card.Title>
                            </Card.Body>
                            <ListGroup className="list-group-flush">
                                <ListGroupItem>
                                    <strong>Last Raced:</strong> {data.date_last_raced}
                                </ListGroupItem>
                                <ListGroupItem>
                                    <strong>Located:</strong> {data.city}, {data.country}
                                </ListGroupItem>

                            </ListGroup>
                        </Card>
                        <br></br>
                        <Card>
                            <Card.Body>
                                <Card.Title class="card-title">Highlights</Card.Title>
                                <Container>
                                    <ReactPlayer url={data.video} width="auto"></ReactPlayer>
                                </Container>
                                <br></br><br></br>
                                <Button class="about-btn" variant="primary" renderAs="button" href={data.video} target="_blank" block>Open Video in New Window</Button>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col md={6}>
                        <Card>
                            <Card.Body>
                                <Card.Title class="card-title">Latest Results</Card.Title>
                            </Card.Body>
                            <ListGroup className="list-group-flush">
                                {data.drivers.map(competitor => (
                                    <ListGroupItem>
                                        <strong>{competitor.result}:</strong> <Link class='d-link' to={'/driver/' + competitor.idDriver}>{competitor.driver}</Link>
                                    </ListGroupItem>
                                ))}
                            </ListGroup>
                        </Card>
                    </Col>
                </Row>
                <br></br>
            </Container>
        );
    }
}
export default Track;