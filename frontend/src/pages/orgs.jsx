import useAxios from "axios-hooks";
import React, { useState, useEffect } from "react";
import { ListGroup, ListGroupItem, Card, CardDeck, Button, ToggleButtonGroup, Pagination, Container } from 'react-bootstrap';
import { BrowserRouter as Router, Route, useParams, Link } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import '../style/custom.css';
import logo from '../logo.png';
const teams = [{ 'id': 0, 'name': 'Mercedes', 'established': '1926', 'team_members': [{ 'name': 'Valtteri Bottas', 'id': 1 }], 'image': 'https://www.thesportsdb.com/images/media/team/badge/tyqwwr1420588072.png' },
{ 'id': 1, 'name': 'McLaren', 'established': '1966', 'team_members': [{ 'name': 'Lando Norris', 'id': 2 }], 'image': 'https://www.thesportsdb.com/images/media/team/badge/xunebq1577469456.png/preview' },
{ 'id': 2, 'name': 'Red Bull', 'established': '2005', 'team_members': [{ 'name': 'Max Verstappen', 'id': 0 }], 'image': 'https://www.thesportsdb.com/images/media/team/badge/vlwzm51519469584.png' }];
function Orgs() {
    const [asc, setOrder] = useState(true);
    const [page, setPage] = useState(1);
    const [sortType, setSortType] = useState('name');
    const [{ data: getOrgs, loading: getOLoading, error: getOError }] = useAxios("/api/orgs");
    const [list, setList] = useState([]);
    let pages = [];
    if (getOrgs != null) {
        var len = getOrgs.length / 6;
        if (getOrgs.length % 6 != 0) {
            len += 1;
        }
        for (let num = 1; num <= len; num++) {
            pages.push(
                <Pagination.Item key={num} active={num === page} onClick={() => setPage(num)}>
                    {num}
                </Pagination.Item>
            )
        }
    }
    useEffect(() => {
        async function sortArray(type) {
            const types = {
                name: 'name',
                year_formed: 'year_formed',
                manager: 'manager',
                location: 'location',
                country: 'country'
            };
            const sortProperty = types[type];
            const sorted = [...getOrgs].sort((a, b) => {
                if (!asc) {
                    var temp = b;
                    b = a;
                    a = temp;
                }
                if (b[sortProperty] > a[sortProperty]) {
                    return -1;
                }
                else if (b[sortProperty] < a[sortProperty]) {
                    return 1;
                }
                else {
                    return 0;
                }
            });
            await setList(sorted.slice((page - 1) * 6, page * 6));
        };
        if (getOrgs != null) {
            sortArray(sortType);
        }
    }, [sortType, getOrgs, page, asc]);
    if (getOLoading) {
        return (<Container className="home_content">
            <img src={logo} alt="Donut logo with racetrack icing" className="App-logo" width="40%" />
            <h1>Loading...</h1>
        </Container>);
    }
    if (getOError) {
        return <p>Error</p>
    }
    return (
        <div>
            <Container className="about_proj home_content">
                <h1>F1 Teams</h1>
                <Card class="col-md-12 text-center">
                    <ToggleButtonGroup type="radio" name='sortOptions' class="row">
                        <Button class="col" variant="secondary" onClick={() => setSortType('name')} value='name'>
                            Name
                        </Button>
                        <Button class="col" variant="secondary" onClick={() => setSortType('year_formed')} value='year_formed'>
                            Year Founded
                        </Button>
                        <Button class="col" variant="secondary" onClick={() => setSortType('location')} value='location'>
                            Location
                        </Button>
                        <Button class="col" variant="secondary" onClick={() => setSortType('country')} value='country'>
                            Country
                        </Button>
                        <Button class="col" variant="secondary" onClick={() => setSortType('manager')} value='manager'>
                            Manager
                        </Button>
                        <Button class="col" variant="secondary" onClick={() => {
                            setOrder(!asc);
                        }} value='reverse'>
                            Reverse Order
                        </Button>
                    </ToggleButtonGroup>
                </Card>
                <br></br>
                <CardDeck class="row col-xs justify-content-center" id="cards">
                    {list.map(team => (
                        <Card className="col-sm-3 gx-3 m-3" style={{ width: 20 + "rem" }} >
                            <Card.Img variant="top" src={team.orgart} alt={team.orgart} style={{backgroundColor: '#F5F5F5'}} />
                            <Card.Body>
                                <Card.Title class="card-title">{team.name}</Card.Title>
                            </Card.Body>
                                <ListGroup className="list-group-flush">
                                    <ListGroupItem>
                                        <strong>Founded:</strong> {team.year_formed}
                                    </ListGroupItem>
                                    <ListGroupItem>
                                        <strong>Manager:</strong> {team.manager}
                                    </ListGroupItem>
                                    <ListGroupItem>
                                        <strong>Located:</strong> {team.location}
                                    </ListGroupItem>
                                    <ListGroupItem>
                                        <strong>Country:</strong> {team.country}
                                    </ListGroupItem>
                                    <ListGroupItem>
                                        <Button class="about-btn" variant="primary" renderAs="button" href={'/team/' + team.idTeam} block>Learn More</Button>
                                    </ListGroupItem>
                                </ListGroup>
                        </Card>
                    )
                    )}
                </CardDeck>
                <Pagination class="text-center">{pages}</Pagination>
            </Container>
        </div>
    );
}
export default Orgs;