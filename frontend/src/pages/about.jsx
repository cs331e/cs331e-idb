import React, { useState, useEffect } from 'react';
import '../style/style.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../style/custom.css';
import { Container, Row, Card, CardDeck, ListGroup, ListGroupItem, Button, CardColumns} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import kevin from '../images/KevinWoo.JPG';
import christina from '../images/ChristinaPeebles.jpg';
import jungwoo from '../images/JJ.jpg';
import theodore from '../images/TheodoreJohnson.jpg';
import eddie from '../images/eddie.jpg';
import useAxios from 'axios-hooks';
import logo from '../logo.png'

export default function About() {
  const [{ data, loading, error }] = useAxios('/api/test')
  const [{data: gitData, loading: gitLoading}] = useAxios('/api/gitlab')
  const [git, setGit] = useState({})
  useEffect(() => {
    setGit(gitData)
  }, [gitData]);
  if (loading || gitLoading){
    return (<Container className="home_content">
      <img src={logo} alt="Donut logo with racetrack icing" className="App-logo" width="40%" />
      <h1>Loading...</h1>
  </Container>);
  }
  if (error){
    return <p>Error</p>
  }
  if (gitData != null && !loading && !gitLoading && data != null){
    return (
      <div className="about_proj home_content">
        <img src={logo} alt="Donut logo with racetrack icing" width="20%" />
        <h1>About the Developers</h1>
        <br></br>
  
        <Container className="about_proj">
          <Row className="justify-content-center">
            <CardDeck>
              <Card style={{ width: '18rem' }}>
                <Card.Img variant="top" src={eddie} />
                <Card.Body>
                  <Card.Title class="card-title">Eddie Castillo</Card.Title>
                  <Card.Subtitle className="mb-2 text-muted">Back-End Developer</Card.Subtitle>
                  <Card.Text>
                    An avid enthusiast in Software Engineering.
                  </Card.Text>
                </Card.Body>
                <ListGroup className="list-group-flush">
                  <ListGroupItem><strong>Num. of Commits: </strong>{gitData['eddie']['commits']}</ListGroupItem>
                  <ListGroupItem><strong>Num. of Issues: </strong>{gitData['eddie']['issues']}</ListGroupItem>
                  <ListGroupItem><strong>Num. of Unit Tests: </strong>{gitData['eddie']['units']}</ListGroupItem>
                  <ListGroupItem>
                    <Button class="about-btn" variant="primary" renderAs="button" href="https://www.cs.utexas.edu/users/bloo280/" target="_blank" block>About</Button>
                    <Button class="about-btn" variant="primary" renderAs="button" href="https://www.linkedin.com/in/eddie-castillo-45085b181/" target="_blank" block>LinkedIn</Button>
                  </ListGroupItem>
                </ListGroup>
              </Card>
  
              <Card style={{ width: '18rem' }}>
                <Card.Img variant="top" src={theodore} />
                <Card.Body>
                  <Card.Title class="card-title">Theodore Johnson</Card.Title>
                  <Card.Subtitle className="mb-2 text-muted">Back-End Developer</Card.Subtitle>
                  <Card.Text>
                    Theodore likes calculators and speakers.
                  </Card.Text>
                </Card.Body>
                <ListGroup className="list-group-flush">
                  <ListGroupItem><strong>Num. of Commits: </strong>{gitData['ted']['commits']}</ListGroupItem>
                  <ListGroupItem><strong>Num. of Issues: </strong>{gitData['ted']['issues']}</ListGroupItem>
                  <ListGroupItem><strong>Num. of Unit Tests: </strong>{gitData['ted']['units']}</ListGroupItem>
                </ListGroup>
                <Card.Body>
                  <Button class="about-btn" variant="primary" renderAs="button" href="https://www.cs.utexas.edu/users/theo/" target="_blank" block>About</Button>
                  <Button variant="primary" renderAs="button" href="https://www.LinkedIn.com/in/TheodoreDrewJohnson/" target="_blank"block>LinkedIn</Button>
                </Card.Body>
              </Card>
  
              <Card style={{ width: '18rem' }}>
                <Card.Img variant="top" src={jungwoo} />
                <Card.Body>
                  <Card.Title class="card-title">Jungwoo Joo</Card.Title>
                  <Card.Subtitle className="mb-2 text-muted">Team Lead</Card.Subtitle>
                  <Card.Text>
                    Mechanical Engineer with an interest in software engineering.
                  </Card.Text>
                </Card.Body>
                <ListGroup className="list-group-flush">
                  <ListGroupItem><strong>Num. of Commits: </strong>{gitData['jungwoo']['commits']}</ListGroupItem>
                  <ListGroupItem><strong>Num. of Issues: </strong>{gitData['jungwoo']['issues']}</ListGroupItem>
                  <ListGroupItem><strong>Num. of Unit Tests: </strong>{gitData['jungwoo']['units']}</ListGroupItem>
                  <ListGroupItem>
                    <Button class="about-btn" variant="primary" renderAs="button" href="https://www.cs.utexas.edu/users/jungwooj/" target="_blank" block>About</Button>
                  </ListGroupItem>
                </ListGroup>
              </Card>
            </CardDeck>
          </Row>
  
          <br></br>
  
          <Row className="justify-content-center">
            <CardDeck>
              <Card style={{ width: '18rem' }}>
                <Card.Img variant="top" src={christina} />
                <Card.Body>
                  <Card.Title class="card-title">Christina Peebles</Card.Title>
                  <Card.Subtitle className="mb-2 text-muted">Front-End Developer</Card.Subtitle>
                  <Card.Text>
                    Christina has let development consume her life.
                  </Card.Text>
                </Card.Body>
                <ListGroup className="list-group-flush">
                  <ListGroupItem><strong>Num. of Commits: </strong>{gitData['christina']['commits']}</ListGroupItem>
                  <ListGroupItem><strong>Num. of Issues: </strong>{gitData['christina']['issues']}</ListGroupItem>
                  <ListGroupItem><strong>Num. of Unit Tests: </strong>{gitData['christina']['units']}</ListGroupItem>
                </ListGroup>
                <Card.Body>
                  <Button class="about-btn" variant="primary" renderAs="button" href="https://www.cs.utexas.edu/users/chrpeebl/" target="_blank" block>About</Button>
                  <Button variant="primary" renderAs="button" href="https://www.linkedin.com/in/christina-peebles-202409173/" target="_blank"block>LinkedIn</Button>
                </Card.Body>
              </Card>
  
              <Card style={{ width: '18rem' }}>
                <Card.Img variant="top" src={kevin} />
                <Card.Body>
                  <Card.Title class="card-title">Kevin Woo </Card.Title>
                  <Card.Subtitle className="mb-2 text-muted">Front-End Developer</Card.Subtitle>
                  <Card.Text>
                    Kevin Woo works hard for no pay.
                  </Card.Text>
                </Card.Body>
                <ListGroup className="list-group-flush">
                  <ListGroupItem><strong>Num. of Commits: </strong>{gitData['kevin']['commits']}</ListGroupItem>
                  <ListGroupItem><strong>Num. of Issues: </strong>{gitData['kevin']['issues']}</ListGroupItem>
                  <ListGroupItem><strong>Num. of Unit Tests: </strong>{gitData['kevin']['units']}</ListGroupItem>
                  <ListGroupItem>
                    <Button class="about-btn" variant="primary" renderAs="button" href="https://www.cs.utexas.edu/users/coolical/" target="_blank" block>About</Button>
                    <Button class="about-btn" variant="primary" renderAs="button" href="https://www.linkedin.com/in/kevin-woo-4210821ba/" target="_blank" block>LinkedIn</Button>
                  </ListGroupItem>
                </ListGroup>
              </Card>
            </CardDeck>
          </Row>
          <br></br>
          <br></br>
          <h1>About the Project</h1>
          <br></br>

          <Row className="justify-content-center">
            <CardColumns>
              <Card>
                <Card.Body>
                  <Card.Title class="card-title">Stats</Card.Title>
                  <Card.Subtitle className="mb-2 text-muted">The work behind our site</Card.Subtitle>
                </Card.Body>
                <ListGroup className="list-group-flush">
                  <ListGroupItem><strong>Total Num. of Commits: </strong>{gitData['commits']}</ListGroupItem>
                  <ListGroupItem><strong>Total Num. of Issues: </strong>{gitData['issues']}</ListGroupItem>
                  <ListGroupItem><strong>Total Num. of Unit Tests: </strong>{gitData['units']}</ListGroupItem>
                </ListGroup>
                <Card.Body>
                  <Button class="about-btn" variant="primary" renderAs="button" href="https://gitlab.com/Jungwoo-Joo-99/cs331e-idb/-/issues" target="_blank" block>Issue Tracker</Button>
                  <Button class="about-btn" variant="primary" renderAs="button" href="https://gitlab.com/Jungwoo-Joo-99/cs331e-idb/-/tree/master" target="_blank" block>Gitlab Repo</Button>
                  <Button class="about-btn" variant="primary" renderAs="button" href="https://gitlab.com/Jungwoo-Joo-99/cs331e-idb/-/wikis/home" target="_blank" block>Gitlab Wiki</Button>
                  <Button class="about-btn" variant="primary" renderAs="button" href="https://speakerdeck.com/coolical/donut-racing" target="_blank" block>Speaker Deck</Button>
                </Card.Body>
              </Card>
              <br></br><br></br>
              <Card>
                <Card.Body>
                  <Card.Title class="card-title ">Data</Card.Title>
                  <Card.Subtitle className="mb-2 text-muted">Links to our data sources</Card.Subtitle>
                </Card.Body>
                <ListGroup className="list-group-flush">
                  <ListGroupItem>
                    <strong><a href="http://ergast.com/mrd/" target="_blank">Ergast</a></strong>
                    <br></br>
                    Querying information over races, drivers, orgs
                  </ListGroupItem>
                  <ListGroupItem>
                    <strong><a href="http://ergast.com/mrd/" target="_blank">SportsDB</a></strong>
                    <br></br>
                    Querying information over races, drivers, orgs
                  </ListGroupItem>
                </ListGroup>
              </Card>
              <br></br><br></br>
              <Card>
                <Card.Body>
                  <Card.Title class="card-title">Tools</Card.Title>
                  <Card.Subtitle className="mb-2 text-muted">The tools behind our site</Card.Subtitle>
                </Card.Body>
                <ListGroup className="list-group-flush">
                  <ListGroupItem>
                    <strong><a href="https://react-bootstrap.github.io/" target="_blank">React Bootstrap</a></strong>
                    <br></br>
                    Tool to make Bootstrap work better with React-App
                  </ListGroupItem>
                  <ListGroupItem>
                    <strong><a href="https://create-react-app.dev/" target="_blank">React App</a></strong>
                    <br></br>
                    Used for frontend
                  </ListGroupItem>
                  <ListGroupItem>
                    <strong><a href="https://reactrouter.com/" target="_blank">React Router</a></strong>
                    <br></br>
                    Used to link React pages to one another
                  </ListGroupItem>
                  <ListGroupItem>
                    <strong><a href="https://flask.palletsprojects.com/en/1.1.x/" target="_blank">Flask</a></strong>
                    <br></br>
                    Used for backend
                  </ListGroupItem>
                </ListGroup>
              </Card>
            </CardColumns>
          </Row>
          <br />
          <Card>
           <Card.Body>
              <Card.Title class="card-title">Unit Tests</Card.Title>
              <Card.Text>{data}</Card.Text>
            </Card.Body>
          </Card>
        </Container>
        <br />
      </div>
    );
  }
}
