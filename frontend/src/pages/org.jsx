import useAxios from "axios-hooks";
import React from "react";
import { ListGroup, ListGroupItem, Container, Row, Col, Image, Card } from 'react-bootstrap';
import { BrowserRouter as Router, Route, useParams, Link } from "react-router-dom";
import logo from '../logo.png'
import 'bootstrap/dist/css/bootstrap.min.css';
import '../style/custom.css';
const teams = [{ 'id': 0, 'name': 'Mercedes', 'established': '1926', 'team_members': [{ 'name': 'Valtteri Bottas', 'id': 1 }], 'image': 'https://www.thesportsdb.com/images/media/team/badge/tyqwwr1420588072.png' },
{ 'id': 1, 'name': 'McLaren', 'established': '1966', 'team_members': [{ 'name': 'Lando Norris', 'id': 2 }], 'image': 'https://www.thesportsdb.com/images/media/team/badge/xunebq1577469456.png/preview' },
{ 'id': 2, 'name': 'Red Bull', 'established': '2005', 'team_members': [{ 'name': 'Max Verstappen', 'id': 0 }], 'image': 'https://www.thesportsdb.com/images/media/team/badge/vlwzm51519469584.png' }]
function Org() {
    const { id } = useParams();
    const [{ data, loading, error }] = useAxios("/api/org/" + id);
    if (loading) {
        return (<Container className="home_content">
            <img src={logo} alt="Donut logo with racetrack icing" className="App-logo" width="40%" />
            <h1>Loading...</h1>
        </Container>);
    }
    if (error) {
        return (<p>Error</p>)
    }
    if (data != null) {
        return (
            <Container className="about_proj home_content">
                <Row className="justify-content-center">
                    <Col md={6}>
                        <Image src={data.orgart} alt={data.orgart} style={{backgroundColor: '#F5F5F5'}} rounded fluid />
                    </Col>
                </Row>
                <h1>{data.name}</h1>
                <br></br>

                <Row>
                    <Col md={6}>
                        <Card>
                            <Card.Body>
                                <Card.Title class="card-title">Team Data</Card.Title>
                            </Card.Body>
                            <ListGroup className="list-group-flush">
                                <ListGroupItem>
                                    <strong>Founded:</strong> {data.year_formed}
                                </ListGroupItem>
                                <ListGroupItem>
                                    <strong>Managed By:</strong> {data.manager}
                                </ListGroupItem>
                                <ListGroupItem>
                                    <strong>Located:</strong> {data.location}
                                </ListGroupItem>
                                <ListGroupItem>
                                    <strong>Country:</strong> {data.country}
                                </ListGroupItem>
                            </ListGroup>
                        </Card>
                    </Col>
                    <Col md={6}>
                        <Card>
                            <Card.Body>
                                <Card.Title class="card-title">Drivers</Card.Title>
                            </Card.Body>
                            <ListGroup className="list-group-flush">
                                {data.drivers.map(member => (
                                    <ListGroupItem>
                                        <Link class='d-link' to={'/driver/' + member.idDriver}>{member.name}</Link>
                                    </ListGroupItem>
                                ))}
                            </ListGroup>
                        </Card>
                    </Col>
                </Row>
                <br></br>
            </Container>
        );
    }
}
export default Org;