import useAxios from "axios-hooks";
import React, { useState, useEffect } from "react";
import { CardDeck, Container, ListGroup, ListGroupItem, Nav, Button, ToggleButtonGroup, Row, Navbar, ButtonGroup, Card, Pagination } from 'react-bootstrap';
import { BrowserRouter as Router, Route, useParams, Link } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import '../style/custom.css';
import { useBootstrapPrefix } from "react-bootstrap/esm/ThemeProvider";
import logo from '../logo.png';
// const drivers = [{ 'id': 0, 'name': 'Max Verstappen', 'birth_loc': 'Hasselt, France', 'birth_yr': '1997', 'team': { 'name': 'Red Bull', 'id': 2 }, 'latest results': [{ 'id': 0, 'str': 'First Place, Abu Dhabi Grand Prix' }, { 'id': 1, 'str': 'Second Place, Bahrain Grand Prix' }, { 'id': 2, 'str': 'Sixth Place, Turkish Grand Prix' }], 'image': 'https://www.thesportsdb.com/images/media/player/cutout/ad31ne1587471775.png' },
// { 'id': 1, 'name': 'Valtteri Bottas', 'birth_loc': 'Nastola, Finland', 'birth_yr': '1989', 'team': { 'name': 'Mercedes', 'id': 0 }, 'latest results': [{ 'id': 0, 'str': 'Second Place, Abu Dhabi Grand Prix' }, { 'id': 1, 'str': 'Eighth Place, Bahrain Grand Prix' }, { 'id': 2, 'str': 'Fourteenth Place, Turkish Grand Prix' }], 'image': 'https://www.thesportsdb.com/images/media/player/cutout/slwoj91587472585.png' },
// { 'id': 2, 'name': 'Lando Norris', 'birth_loc': 'Bristol, England', 'birth_yr': '1999', 'team': { 'name': 'McLaren', 'id': 1 }, 'latest results': [{ 'id': 0, 'str': 'Fifth Place, Abu Dhabi Grand Prix' }, { 'id': 1, 'str': 'Fourth Place, Bahrain Grand Prix' }, { 'id': 2, 'str': 'Eighth Place, Turkish Grand Prix' }], 'image': 'https://www.thesportsdb.com/images/media/player/cutout/0t83vs1587468873.png' }];

function Drivers() {
    const [asc, setOrder] = useState(true);
    const [page, setPage] = useState(1);
    const [sortType, setSortType] = useState('name');
    const [{ data: getDrivers, loading: getDLoading, error: getDError }] = useAxios("/api/drivers");
    const [list, setList] = useState([]);
    let pages = [];
    if (getDrivers != null) {
        var len = getDrivers.length / 6;
        if (getDrivers.length % 6 != 0) {
            len += 1;
        }
        for (let num = 1; num <= len; num++) {
            pages.push(
                <Pagination.Item key={num} active={num === page} onClick={() => setPage(num)}>
                    {num}
                </Pagination.Item>
            )
        }
    }
    useEffect(() => {
        async function sortArray(type) {
            const types = {
                name: 'name',
                nationality: 'nationality',
                gender: "gender",
                organization: "team",
                birthday: "birthday",
                birth_loc: "birth_loc"
            };
            const sortProperty = types[type];
            const sorted = [...getDrivers].sort((a, b) => {
                if (!asc) {
                    var temp = b;
                    b = a;
                    a = temp;
                }
                if (sortProperty === 'team') {
                    if (b[sortProperty]['name'] > a[sortProperty]['name']) {
                        return -1;
                    }
                    else if (b[sortProperty]['name'] < a[sortProperty]['name']) {
                        return 1;
                    }
                    else {
                        return 0;
                    }
                }
                else {
                    if (b['driver'][sortProperty] > a['driver'][sortProperty]) {
                        return -1;
                    }
                    else if (b['driver'][sortProperty] < a['driver'][sortProperty]) {
                        return 1;
                    }
                    else {
                        return 0;
                    }
                }
            });
            console.log(sorted)
            await setList(sorted.slice((page - 1) * 6, page * 6));
        };
        if (getDrivers != null) {
            sortArray(sortType);
        }
    }, [sortType, getDrivers, page, asc]);
    if (getDLoading) {
        return (<Container className="home_content">
            <img src={logo} alt="Donut logo with racetrack icing" className="App-logo" width="40%" />
            <h1>Loading...</h1>
        </Container>);
    }
    if (getDError) {
        return <p>Error</p>
    }
    return (
        <div>
            <Container className="about_proj home_content">
                <h1>F1 Drivers</h1>
                <Card class="text-center">
                    <ToggleButtonGroup variant="primary" type="radio" name='sortOptions' class="row">
                        <Button class="col" onClick={() => setSortType('name')} value='name' variant="secondary">
                            Name
                        </Button>
                        <Button class="col" onClick={() => setSortType('nationality')} value='nationality' variant="secondary">
                            Nationality
                        </Button>
                        <Button class="col" onClick={() => setSortType('gender')} value='gender' variant="secondary">
                            Gender
                        </Button>
                        <Button class="col" onClick={() => setSortType('birthday')} value='birthday' variant="secondary">
                            Birthday
                        </Button>
                        <Button class="col" onClick={() => setSortType('birth_loc')} value='birth_loc' variant="secondary">
                            Birth Location
                        </Button>
                        <Button class="col" onClick={() => setSortType('organization')} value='team' variant="secondary">
                            Team
                        </Button>
                        <Button class="col" onClick={() => {
                            setOrder(!asc);
                        }} value='reverse' variant="secondary">
                            Reverse Order
                        </Button>
                    </ToggleButtonGroup>
                </Card>
                <br></br>
                <CardDeck class="row col-xs justify-content-center" id="cards">
                    {list.map(driver => (
                        <Card className="col-sm-3 gx-3 m-3" key={driver['driver'].id} style={{ width: 20 + "rem" }} >
                            <Card.Img variant="top" src={driver['driver'].driverart} alt={driver['driver'].driverart} />
                            <Card.Body>
                                <Card.Title class="card-title">{driver['driver'].name}</Card.Title>
                                <Link class='d-link' to={'team/' + driver['driver'].idTeam}>{driver['team'].name}</Link>
                            </Card.Body>
                            <ListGroup className="list-group-flush">
                                <ListGroupItem>
                                    <strong>Nationality:</strong> {driver['driver'].nationality}
                                </ListGroupItem>
                                <ListGroupItem>
                                    <strong>Gender:</strong> {driver['driver'].gender}
                                </ListGroupItem>
                                <ListGroupItem>
                                    <strong>Born:</strong> {driver['driver'].birth_loc}; {driver['driver'].birthday}
                                </ListGroupItem>
                                <ListGroupItem>
                                    <Button class="about-btn" variant="primary" renderAs="button" href={'/driver/' + driver['driver'].idDriver} block>Biography</Button>
                                </ListGroupItem>
                            </ListGroup>
                        </Card>
                    )
                    )}
                </CardDeck>
                <Pagination sticky="bottom" class="text-center">{pages}</Pagination>
            </Container>
        </div>
    );
}
export default Drivers;