#{'name':,'birth_loc':,'birth_yr':,'team':,'latest_results':}
drivers = [{'name':'Max Verstappen','birth_loc':'Hasselt, France','birth_yr':'1997','team':'Red Bull','latest results':['First Place, Abu Dhabi Grand Prix','Second Place, Bahrain Grand Prix','Sixth Place, Turkish Grand Prix']},\
          {'name':'Valtteri Bottas','birth_loc':'Nastola, Finland','birth_yr':'1989','team':'Mercedes','latest results':['Second Place, Abu Dhabi Grand Prix','Eighth Place, Bahrain Grand Prix','Fourteenth Place, Turkish Grand Prix']},\
          {'name':'Lando Norris','birth_loc':'Bristol, England','birth_yr':'1999','team':'McLaren','latest results':['Fifth Place, Abu Dhabi Grand Prix','Fourth Place, Bahrain Grand Prix','Eighth Place, Turkish Grand Prix']}]
#{'name':,'loc':,'date':,'circuit':,'competitors':[x['name'] for x in drivers]}
events = [{'name':'Abu Dhabi Grand Prix','loc':'Abu Dhabi, UAE','date':'12/13/20','circuit':'Yas Marina Circuit','competitors':[x['name'] for x in drivers]}, \
         {'name':'Bahrain Grand Prix','loc':'Sakhir, Bahrain','date':'11/29/20','circuit':'Bahrain International Circuit','competitors':[x['name'] for x in drivers]}, \
         {'name':'Turkish Grand Prix','loc':'Istanbul, Turkey','date':'11/15/20','circuit':'Intercity Istanbul Park','competitors':[x['name'] for x in drivers]}]
#{'name':,'established':,'team_members':}
teams = [{'name':'Mercedes','established':'1926','team_members':[x['name'] for x in drivers if x['team']=='Mercedes']}, \
         {'name':'McLaren','established':'1966','team_members':[x['name'] for x in drivers if x['team']=='McLaren']}, \
         {'name':'Red Bull','established':'2005','team_members':[x['name'] for x in drivers if x['team']=='Red Bull']} ]

# The project instructions mention that there has to be some order to items
def organize_items(item_arr, key, ascending):
    if ascending:
        return sorted(item_arr, key = lambda x:(x[key]))
    else:
        return sorted(item_arr, key = lambda x:(x[key]), reverse = True)
